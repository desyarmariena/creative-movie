# Creative Movie

This project was bootstrapped with
[Create React App](https://github.com/facebook/create-react-app) and using OMDb
API.

## System Requirements

- [git][git] v2.13 or greater
- [NodeJS][node] `12 || 14 || 15`
- [npm][npm] v6 or greater

All of these must be available in your `PATH`. To verify things are set up
properly, you can run this:

```shell
git --version
node --version
npm --version
```

## Setup

```
git clone https://gitlab.com/desyarmariena/creative-movie.git
cd creative-movie
yarn install
```

### `Running the app`

Before you run the app, you need to change env file into .env and add your own
OMDb API key\
After that, you can run

```shell
yarn start
```

### `Future Improvement`

This project still lack a lot considering limited time constrain. It could
developed more, like

- Add placeholder
- Better design
- Add videos for trailer
- Offline feature
- and many more

# Logic Test

The challange was to group words from given array of strings and return new
array with grouped anagram words in it.

```Javascript
// input:
['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']
// expected output:
[
 ["kita", "atik", "tika"],
 ["aku", "kua"],
 ["makan"],
 ["kia"],
]
```

Anagram is a term for a word that formed by rearranging the letters into another
word.

Some key that I catched here:

- Words that grouped as anagram must have the same character length.
- The amount of each character must be the same also

## Anagram Example:

elbow and below is an anagram.\
state and taste is an anagram.

## Non-Anagram Example:

**tried and cider not an anagram.**

Even though tried and cider both have 5 charachers each, they're not anagram.\
Because the word **tried** has 1 'T" character, but **cider** word doesn't have that
character.\
Vice versa for word **cider** that has 1 'C' character, and **tried** doesn't
have it.

## Algorithm

The idea behind my algorithm below is to sort every words into alphabetic
sequence.\
After the words sorted, we're safe to loop and match every character from start to
end of **two same length words** and grouped it accordingly.

```Javascript
const words = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']

function sortWord(word) {
	const splittedWord = word.split('');
  for (let i = 0; i < splittedWord.length; i++) {
    const currentChar = splittedWord[i]

    for (let j = i+ 1; j < splittedWord.length; j++) {
      const nextChar = splittedWord[j]
      // swap
      if (splittedWord[i] > nextChar) {
        const temp = splittedWord[i];
        splittedWord[i] = nextChar;
        splittedWord[j] = temp;
      }
    }

  }
  return splittedWord.join('')
}

function compareSortedWords(word1, word2) {
	if (word1.length !== word2.length) return false;

  var index = 0;
  var sameChar = word1[index] === word2[index]
  while (sameChar && index < word1.length) {
  	index++;
    sameChar =  word1[index] === word2[index]
  }
  return sameChar
}



function groupWordsByAnagram(words = []) {
	const anagrams = []
	anagrams.push([words[0]])

	for (let i = 1; i < words.length; i++) {
    var checkAnagram = false
    for (let j = 0; j < anagrams.length; j++) {
      const firstWord = sortWord(words[i])
      const secondWord = sortWord(anagrams[j][0])

      checkAnagram = compareSortedWords(firstWord, secondWord)
      if (checkAnagram) {
        anagrams[j].push(words[i])
        break;
      }
    }
    if (!checkAnagram) {
       anagrams.push([words[i]])
    }
  }
  return anagrams
}

console.log('Anagrams grouped:', groupWordsByAnagram(words))
```

<!-- prettier-ignore-start -->
[npm]: https://www.npmjs.com/
[node]: https://nodejs.org
[git]: https://git-scm.com/

<!-- prettier-ignore-end -->
