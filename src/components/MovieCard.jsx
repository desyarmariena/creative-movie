import React from 'react'
import {Link} from 'react-router-dom'
import styles from './MovieCard.module.scss'

export default React.forwardRef(function MovieCard({movie}, ref) {
  return (
    <div className={styles.container} ref={ref}>
      <Link to={`/movie/${movie.imdbID}`}>
        <div className={styles.poster}>
          <img src={movie.Poster} alt={movie.title} />
        </div>
        <div className={styles.info}>
          <div>
            <p className="title">
              {movie.Title} <span>({movie.Year})</span>
            </p>
          </div>
        </div>
      </Link>
    </div>
  )
})
