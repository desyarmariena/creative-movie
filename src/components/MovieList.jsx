import React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {fetchMoreMovies} from '../state/reducers/movie'
import MovieCard from './MovieCard'
import styles from './MovieList.module.scss'

export default function MovieList() {
  const {movies, status, hasNextPage} = useSelector(state => state)
  const dispatch = useDispatch()
  const observer = React.useRef()

  const lastMovieContainer = React.useCallback(
    node => {
      if (status === 'loading') return
      if (observer.current) observer.current.disconnect()
      observer.current = new IntersectionObserver(entries => {
        if (entries[0].isIntersecting && hasNextPage) {
          dispatch(fetchMoreMovies()).unwrap()
        }
      })
      if (node) observer.current.observe(node)
    },
    [status, dispatch, hasNextPage],
  )

  if (status === 'idle') {
    return (
      <div className={styles.centerContainer}>
        <p>Hi there! What movie you want to search today?</p>
      </div>
    )
  }

  if (movies.length === 0 && status !== 'loading') {
    return (
      <div className={styles.centerContainer}>
        <p>We cannot find your movie. Maybe try other?</p>
      </div>
    )
  }

  return (
    <div>
      <div className={styles.movieGrid}>
        {movies.map((movie, index) => {
          if (index + 1 < movies.length) {
            return <MovieCard key={movie.imdbID} movie={movie} />
          } else {
            return (
              <MovieCard
                ref={lastMovieContainer}
                className="last"
                key={movie.imdbID}
                movie={movie}
              />
            )
          }
        })}
      </div>
      {status === 'loading' && (
        <div className={styles.centerContainer}>
          <p>Fetching movies...</p>
        </div>
      )}
    </div>
  )
}
