import React, {useState} from 'react'
import {useDispatch} from 'react-redux'
import {fetchMoviesByTitle} from '../state/reducers/movie'
import styles from './MovieFilter.module.scss'

const suggestions = ['batman', 'captain america', 'cruella', 'harry potter']
export default function MovieFilter() {
  const [search, setSearch] = useState('')
  const dispatch = useDispatch()
  const [showSuggestion, setShowSuggestion] = useState(false)

  const onClickSuggestion = keyword => {
    setSearch(keyword)
    setShowSuggestion(false)
  }

  const onHideSuggestion = () => {
    setTimeout(() => {
      setShowSuggestion(false)
    }, 300)
  }

  const onSearchMovie = async () => {
    if (!search) return
    try {
      await dispatch(fetchMoviesByTitle({title: search, page: 1})).unwrap()
    } catch (err) {
      console.log('error', err)
    }
  }

  return (
    <div className={styles.formContainer}>
      <form onSubmit={e => e.preventDefault()}>
        <div className={styles.searchContainer}>
          <input
            className={styles.searchInput}
            type="search"
            value={search}
            onChange={e => setSearch(e.target.value)}
            placeholder="Search movie..."
            onFocus={e => setShowSuggestion(true)}
            onBlur={onHideSuggestion}
          />
          <button type="submit" onClick={onSearchMovie}>
            Search
          </button>
          {showSuggestion && (
            <ul className={styles.autoComplete}>
              {suggestions
                .filter(word => word.includes(search))
                .map(item => {
                  return (
                    <li key={item} onClick={() => onClickSuggestion(item)}>
                      {item}
                    </li>
                  )
                })}
            </ul>
          )}
        </div>
      </form>
    </div>
  )
}
