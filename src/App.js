import React from 'react'
import './App.scss'
import {Redirect, Route, Switch} from 'react-router-dom'
const MoviesPage = React.lazy(() =>
  import(/* webpackPrefetch: true */ './pages/Movies'),
)
const MovieDetailPage = React.lazy(() => import('./pages/MovieDetail'))

function App() {
  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <Switch>
        <Route exact path="/movies" render={() => <MoviesPage />} />
        <Route path="/movie/:id" render={() => <MovieDetailPage />} />
        <Redirect from="*" to="/movies" />
      </Switch>
    </React.Suspense>
  )
}

export default App
