import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import {Link} from 'react-router-dom'
import axios from 'axios'
import styles from '../styles/MovieDetail.module.scss'
import {ReactComponent as Home} from '../assets/home.svg'

export default function MovieDetail() {
  const {id} = useParams()
  const [movie, setMovie] = useState()

  useEffect(() => {
    axios
      .get(
        `http://www.omdbapi.com/?apikey=${process.env.REACT_APP_MOVIE_KEY}&i=${id}&plot=full`,
      )
      .then(({data}) => {
        setMovie(data)
      })
      .catch(error => {
        console.log('error', error)
      })
  }, [id])

  return (
    <div className="container">
      <div className={styles.card}>
        {movie && (
          <>
            <Link to={`/movies`} className={styles.link}>
              <Home size="1rem" />
              <span>Home</span>
            </Link>
            <div className={styles.container}>
              <div className={styles.poster}>
                <img src={movie.Poster} alt={`poster ${movie.Title}`} />
              </div>
              <div className={styles.info}>
                <h1>
                  {movie.Title} <span>({movie.Year})</span>
                </h1>
                <p>
                  <span>IMDb rating</span>: {movie.imdbRating}/10
                </p>
                <p>
                  <span>Production House</span>: {movie.Production}
                </p>
                <p>
                  <span>Genre</span>: {movie.Genre}
                </p>
                <p>
                  <span> Cast</span>: {movie.Actors}
                </p>
                <div>
                  <h3>Plot</h3>
                  <p>{movie.Plot}</p>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  )
}
