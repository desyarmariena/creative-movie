import React from 'react'
import MovieFilter from '../components/MovieFilter'
import MovieList from '../components/MovieList'

export default function Movies() {
  return (
    <div className="container">
      <MovieFilter />
      <MovieList />
    </div>
  )
}
