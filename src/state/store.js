import {configureStore} from '@reduxjs/toolkit'
import rootReducer from './reducers/movie'

export default configureStore({
  reducer: rootReducer,
})
