import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import axios from 'axios'

export const fetchMoviesByTitle = createAsyncThunk(
  'movie/fetchByTitle',
  async ({title, page = 1}, {getState, rejectWithValue}) => {
    const {lastFetchedQuery} = getState()
    if (!title && lastFetchedQuery.title) {
      title = lastFetchedQuery.title
    }
    const {data} = await axios.get('http://www.omdbapi.com', {
      params: {
        apikey: process.env.REACT_APP_MOVIE_KEY,
        s: title,
        page,
      },
    })
    if (data.Response === 'False') {
      return rejectWithValue(data.Error || 'Error', {})
    }
    return {movies: data.Search, total: +data.totalResults}
  },
)

export const fetchMoreMovies = createAsyncThunk(
  'movie/fetchMoreByTitle',
  async (state, {getState, rejectWithValue}) => {
    const {lastFetchedQuery} = getState()
    if (!lastFetchedQuery.page) return
    const {data} = await axios.get('http://www.omdbapi.com', {
      params: {
        apikey: process.env.REACT_APP_MOVIE_KEY,
        s: lastFetchedQuery.title,
        page: lastFetchedQuery.page + 1,
      },
    })
    if (data.Response === 'False') {
      return rejectWithValue(data.Error || 'Error', {})
    }
    return {movies: data.Search, total: +data.totalResults}
  },
)

export const movieSlice = createSlice({
  name: 'movie',
  initialState: {
    movies: [],
    status: 'idle', // idle, loading, success, error
    movieDetail: {},
    hasNextPage: false,
    currentPage: 0,
    lastFetchedQuery: {},
  },
  reducers: {},
  extraReducers: {
    [fetchMoviesByTitle.pending]: state => {
      state.status = 'loading'
      state.movies = []
    },
    [fetchMoviesByTitle.fulfilled]: (state, {payload, ...rest}) => {
      const {page} = rest.meta.arg
      state.lastFetchedQuery = rest.meta.arg
      const pages = Math.ceil((payload.total - 1) / 10)
      state.currentPage = page
      state.movies = [...payload.movies]
      if (pages > page) {
        state.hasNextPage = true
      } else {
        state.hasNextPage = false
      }
      state.status = 'success'
    },
    [fetchMoviesByTitle.rejected]: (state, {payload}) => {
      state.status = 'error'
      state.movies = []
    },
    [fetchMoreMovies.pending]: state => {
      state.status = 'loading'
    },
    [fetchMoreMovies.fulfilled]: (state, {payload, ...rest}) => {
      state.lastFetchedQuery.page += 1
      state.currentPage = state.lastFetchedQuery.page

      const pages = Math.ceil((payload.total - 1) / 10)
      if (pages > state.currentPage) {
        state.hasNextPage = true
      } else {
        state.hasNextPage = false
      }

      state.movies = [...state.movies, ...payload.movies]
      state.status = 'success'
    },
    [fetchMoreMovies.rejected]: (state, {payload}) => {
      state.status = 'error'
      state.movies = []
    },
  },
})

export default movieSlice.reducer
